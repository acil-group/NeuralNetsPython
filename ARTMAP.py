#Author: Niklas Melton
#Date: 09/02/2016
#Online FuzzyARTMAP implemention.

import numpy as np
from NeuralNets import FuzzyART as ART


class ARTMAP:
    def __init__(self,rho,alpha,beta,wA=None,wB=None,wMap = None):
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.map = wMap
        self.A = ART.ARTnet(rho,alpha,beta,wA)
        self.B = ART.ARTnet(rho,alpha,beta,wB)


    def supervised(self, dataA, dataB, getB = False):
        mapped = False
        #determine which ART B cluster matches the data
        clusterB = self.B.learnRun(dataB)
        while(not(mapped)):
  
            #determine which ART A cluster matches the data
            clusterA = self.A.learnRun(dataA)
            
            if self.map is None:
                #initialize map weights if none exist
                self.map = np.zeros((self.A.clustNum, self.B.clustNum))
                
            #if the chosen cluster exists outside of map weight space, expand
            #the map weights to fit the new clusters
            if clusterA > (len(self.map)-1):
                z = np.zeros(len(self.map[0]))
                self.map = np.vstack((self.map,z))

            if clusterB > (len(self.map[0])-1):
                z = np.zeros((len(self.map),1))
                self.map = np.concatenate((self.map,z), 1)

            #check map 
            if not(np.any(self.map[clusterA] > 0)):
                if dataB is not None:
                    self.map[clusterA][clusterB] = 1
                mapped = True
            #use what is known 
            elif dataB is None:
                clusterB = np.argmax(self.map[clusterA])
                mapped = True
            #match tracking    
            elif self.map[clusterA][clusterB] != 1:
                Ax = np.concatenate((dataA, 1-dataA))
                self.A.rho = (ART.norm(ART.fuzzyAnd(Ax, self.A.w[clusterA]))/ART.norm(Ax)) + 0.01
            else:
                mapped = True
                
        self.A.rho = self.rho

        if getB is True:
            return clusterB


    def runForward(self, dataA, getW=True):
        clusterA = self.A.run(dataA)
        clusterB = np.argmax(self.map[clusterA])
        if getW is True:
            if type(self.B) is FuzzyART.ARTnet:
                if (len(self.B.w.shape) == 1 and clusterB == 0):
                    dataB =  self.B.w
                else:
                    dataB =  self.B.w[clusterB]
            else:
                if (len(self.B.ART.w.shape) == 1 and clusterB == 0):
                    dataB =  self.B.ART.w
                else:
                    dataB =  self.B.ART.w[clusterB]
            dataB = dataB[:len(dataB)/2]
            return dataB
        else:
            return clusterB
    

    def runBack(self, dataB):
        if type(self.B) is FuzzyART.ARTnet:
            clusterB = self.B.run(dataB)
        else:
            clusterB = self.B.run(dataB)[0]
        clusterA = np.argmax(self.map[:,clusterB])
        if (len(self.A.w.shape) == 1 and clusterA == 0):
            dataA = self.A.w
        else:
            dataA = self.A.w[clusterA]
        return dataA
        
        
        
        
        
        
            
        
        
        
        
        
        
        
        
        
        
        

