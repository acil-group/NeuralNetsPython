#Author: Niklas Melton
#Date: 16/03/2016
#FuzzyART test.

from NeuralNets import FuzzyART as ART
import numpy as np
import matplotlib.pyplot as plt

dataSize = 30
dataWidth = 8
dataThresh = 0.6

rho = 0.7
alpha = 0.1
beta = 0.9

#create random test sets
data = np.zeros(dataSize, dtype=object)
for i in range(0, dataSize):
    data[i] = np.random.uniform(0,1,dataWidth)
    data[i][(data[i]<dataThresh)] = 0
    data[i][(data[i]>=dataThresh)] = 1
    #print(data[i])
    
    
ARTa = ART.ARTnet(rho, alpha, beta)
for i in range(0, 3):
    for j in range(0, dataSize):
        ARTa.learnRun(data[j])
print('Number of data:',dataSize)
print('Number of clusters:', ARTa.clustNum)
clusters = np.zeros(ARTa.clustNum, dtype=object)
for i in range(0, dataSize):
    T = ARTa.run(data[i])
    if type(clusters[T]) == int:
        clusters[T] = np.copy(data[i])
    else:
        clusters[T] = np.vstack((clusters[T],data[i]))


plt.close('all')
plots = np.zeros(ARTa.clustNum, dtype = object)
x = range(dataWidth)
for t in range(0, ARTa.clustNum):
    print(clusters[t], t)
    if len(clusters[t]) == dataWidth:
        f, plots[t] = plt.subplots(1, sharex=True)
        plots[t].set_title('Cluster {}'.format(t))
    else:
        f, plots[t] = plt.subplots(len(clusters[t]), sharex=True)
        plots[t][0].set_title('Cluster {}'.format(t))
    if np.size(clusters[t],0) == dataWidth:
        plots[t].bar(x, clusters[t], width=0.4)
    else:
        for i in range(0, len(clusters[t])):
            plots[t][i].bar(x, clusters[t][i], width=0.4)

plt.show()
                   
