#Author: Niklas Melton
#Date: 04/04/2016
#HDP testing code

import pyDrive, EnhancedSL as ESL, MLP, pygame, sys, time, math, numpy as np, NeuralDraw as nd
from pygame.locals import *



carSize = 40,20
senseNum = 7
speed = 1.0
steer = 1
Hsteer = 0.5
steerRange = math.radians(60)
sRange = 100
LR = 0.1
MR = 0.0
gamma = 0.1

green = 0,255,0
red = 255,0,0

K = 0

def utility(X):
    cost = 0
    costMax = 0
    n = len(X)
    for i in range(0, int(n/2)):
        cost += np.sqrt(abs(X[i]*X[i]-X[n-1-i]*X[n-1-i]))
        costMax+=1
    cost += 0.2*np.sum((1-X))
    costMax += 0.2*n
    
    return np.copy(cost/costMax)


joyBias = 0;
def centerJoy(j):
    global joyBias
    if abs(j) < 0.1:
        joyBias = 0.005*j+ 0.995*joyBias
    j-=joyBias
    return min(1.0,max(-1.0,j))
             

trackModel = MLP.loadNetwork('model.txt')
trackCritic = MLP.loadNetwork('critic.txt')
HDPshape = [[senseNum,7,1],trackModel.shape,trackCritic.shape]
A = ESL.ESLnet(HDPshape, utility, model = trackModel, critic = trackCritic, growThresh = 0.1)

my_course = pyDrive.course('stadium.txt')
screenSize = my_course.screenSize
startPos = my_course.startPos
my_vehicle = pyDrive.car(carSize, startPos)
my_env = pyDrive.env(screenSize, my_vehicle, my_course, senseNum, sRange)
my_env.update(speed,0)

sd = nd.subDisplay(my_env.screen,screenSize[0],screenSize[0]+300, 0, screenSize[1], 20)
sdnetA = nd.network(A.HDP.actor)
sdnetM = nd.network(A.HDP.model)
sdnetC = nd.network(A.HDP.critic)
sd.pushDrawList(sdnetA)
sd.pushDrawList(sdnetM)
sd.pushDrawList(sdnetC)

go = 0
learnChoice = 0
print('----INPUT REQUIRED-----')
print('Controller options')
print('1. xBox controller')
print('2. Keyboard')
controlChoice = int(input('Controller Choice:'))
if controlChoice == 1:
    #Tells the number of joysticks/error detection
    joystick_count = pygame.joystick.get_count()
    print ("There is ", joystick_count, "joystick/s")
    if joystick_count == 0:
        print ("Error, I did not find any joysticks")
    else:
        print('Joystick found')
        my_joystick = pygame.joystick.Joystick(0)
        my_joystick.init()
K = float(input('\n\nEnter K: '))
print('Press A to play')
 
while True:
    while go != 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    go = 1
        if controlChoice == 1:
            if my_joystick.get_button(0) == 1:
                go = 1
                time.sleep(1)
            
    while my_env.crashed == False and my_env.JK == False:
     
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event .type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and steer > 0:
                    steer -= 0.1*steerRange
                if event.key == pygame.K_RIGHT and steer < 1:
                    steer += 0.1*steerRange
                if event.key == pygame.K_a:
                    go = 0
        if controlChoice == 1:
            if my_joystick.get_button(0) == 1:
                go = 0
                print('Paused. Press A to continue, Y for options')
                
            
        while go == 0:
            #game paused
            time.sleep(1)
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
                if event .type == pygame.KEYDOWN:
                    if event.key == pygame.K_a:
                        go = 1
                    if event.key == pygame.K_y:
                        print('Learning Options')
                        print('1. Change K value')
                        print('2. Demonstrate Policy')
                        print('3. Save Actor')
                        learnChoice = int(input('Choice: '))
                        if learnChoice == 1:
                            K = float(input('Enter New K: '))
                        if learnChoice == 3:
                            A.actor.saveNetwork('SLactor.txt')
                            print('saved')
                        print('Paused. Press A to continue, Y for options')
                        
            if controlChoice == 1 :
                if my_joystick.get_button(0) == 1:
                    go = 1
                    time.sleep(0.3)
                if my_joystick.get_button(1) == 1:
                    if learnChoice != 3:
                        learnChoice = 3
                    else:
                        learnChoice = 0;
                    go = 1
                    time.sleep(0.3)
                if my_joystick.get_button(2) == 1:
                    print('Learning Options')
                    print('1. Change K value')
                    print('2. Save Actor')
                    if learnChoice != 3:
                        print('3. Demonstrate Policy')
                        learnChoice = int(input('Choice:'))
                    else:
                        print('3. Train Policy')
                        learnChoice = int(input('Choice:'))
                        if learnChoice == 3:
                            learnChoice = 0
                    
                    if learnChoice == 2:
                            A.actor.saveNetwork('SLactor.txt')
                            print('saved')
                    
                    if learnChoice == 1:
                        K = float(input('Enter New K: '))
                    print('Paused. Press A to continue, B for options')
          
    
        if controlChoice == 1:
            Hsteer = MLP.remap(centerJoy(my_joystick.get_axis(0)),-1.0,1.0,0.004,0.996)

        X = my_env.getSense()/sRange
        if learnChoice != 3:
            if K < 1:
                A.learnPass(np.array(X),np.array([Hsteer]),K,LR,MR,gamma)
                steer = Hsteer
            else:
                steer = A.learnPass(np.array(X),np.array([Hsteer]),K,LR,MR,gamma, getA = True)
        else:
            steer = A.HDP.actor.feedForward(X)
        steer = (steer-0.5)*steerRange
        #print(steer, Hsteer)
        if learnChoice == 3:
            carColor = green
        else:
            carColor = red
        my_env.update(speed,steer, vehicleColor = carColor,doFlip =  False)
        pygame.draw.circle(my_env.screen,(0,255,0),(int(my_env.vehicle.X),int(my_env.vehicle.Y)),5)
        sd.draw(5)
        pygame.display.flip()
        
    print('------crashed------\n')
    if K == 1:
        go = 1
    else:
        print('Press A to play again')
        time.sleep(1)
        go = 0
        time.sleep(1)
    my_env.reset()
    



          
          
