#Author: Niklas Melton
#Date: 16/04/2016
#Enhanced Supervised Learning algorithm by means of HDP

import numpy as np
from NeuralNets import MLP, HDP

def defult_func(K, A, dA, S, dS)
    #find supervised learning error
    dSL = A-S
    #get new error signal
    dNew = K*dA + (1-K)*(dSL + K*dS)
    return = np.copy(dNew)

def newton(K, A, dA, S, dS):
    #find supervised learning error
    dSL = A-S;
    #approximate second error derivative by FDM
    ddE = (dA-dS)/(A-S)
    #use newtons method (with concavity check) to approximate control signal with min error
    Anewt = A - dA/abs(ddE)
    #treat this approximation as a supervised label
    dSLnewt = A-Anewt

    #combine supervised and reinforcement methods
    dNew = K*dSLnewt + (1-K)*dSL
    return  dNew
    
    

class ESLnet:
    def __init__(self, shape, utility, model = None, actor = None, critic = None, growThresh = None, filterFunc = None):
        self.HDP = HDP.HDPnet(shape, utility, model, actor, critic, growThresh)
        if filterFunc = None:
            filterFunc = default_func
        self.filterFunc = filterFunc
            

    def learnPass(self, X, S, K, LR, MR, gamma=0.3, getA = True, trainModel = False, selfControl =True):
        #runs one learning pass of the HDP network
        #maxK determines the number of timesteps into the future we look
        dA = np.zeros(len(np.array(S)))
        dS = np.zeros(len(np.array(S)))
        dSL = np.zeros(len(np.array(S)))
        #preserve state information for critic
        Xlast = self.HDP.Xlast
        #Do actor side learning for so many cycles
        for nA in range(0, self.HDP.cycleA):
            #Run Actor
            A = self.HDP.actor.feedForward(X)
            #Run HDP side
            dA = HDP.judgeAction(A, X, LR, MR, gamma, trainCritic = False, trainModel = False)
            dS = HDP.judgeAction(S, X, LR, MR, gamma, trainCritic = False, trainModel = False)
            #Filter Equation
            dNew = self.filterFunc(K, A, dA, S, dS)
            ##print("A:",A,"  dA:", dA,"  S:",S,"  dS:", dS,"  dSL:", dSL, "  dNew:",dNew)
            #backprop error signal and learn
            dX = self.HDP.actor.backProp(dNew, LR, MR)
        if K > 0:
            #run critic side learning for so many cycles
            for nC in range(0,self.HDP.cycleC):
                self.HDP.Xlast = Xlast
                if selfControl:
                    S = A
                dS = self.HDP.judgeAction(S, X, LR, MR, gamma, trainCritic = True, trainModel = trainModel)
        if getA == True:
            return A
