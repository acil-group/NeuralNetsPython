#Author: Niklas Melton
#Date: 09/02/2016
#FuzzyART implemention.

import numpy as np

def norm(x):
#determine input norm
    normX = 0
    for i in range(0,len(x)):
        normX += abs(x[i])
    return float(normX)


def fuzzyAnd(x, y):
    return np.minimum(x,y)

class ARTnet:
    def __init__(self, rho, alpha, beta, weights=None):
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.w = weights
        self.clustNum = 0
        self.i = []

    def learnRun(self, data, getCluster = True):
        if data is None:
            if np.isnan(self.w).any():
                return np.where(np.isnan(self.w))[0][0]
            #new category node needs to be created
            self.w = np.vstack([self.w, np.nan])
            self.clustNum = len(self.w)
            #forces new node selection
            if getCluster == True:
                return self.clustNum-1
            else:
                return
        inputVector = np.concatenate((data, 1-data))
        if self.w is None:
            self.w = np.array([inputVector])
            self.clustNum = 1
            self.i.append(0)
            if getCluster == True:
                return 0
            else:
                return
        #determine input norm and match criterion
        normInput = norm(inputVector)
        MC = normInput*self.rho
        #Choose a category node
        T = np.zeros(self.clustNum)
        for j in range(0, self.clustNum):
            T[j] = norm(fuzzyAnd(inputVector,self.w[j]))/(self.alpha+norm(self.w[j]))
        #test each category node selected until the vigilance criteria is met
        hyp = 0
        while (hyp < MC):
            Twin = np.argmax(T)
            hyp = norm(fuzzyAnd(inputVector, self.w[Twin]))
            if hyp < MC:
                T[Twin] = 0
            if norm(T) == 0:
                if np.isnan(self.w).any():
                    Twin = np.where(np.isnan(self.w))[0][0]
                    self.w[Twin] = np.array([inputVector])
                    T[Twin] = 1
                else:
                    #new category node needs to be created
                    self.w = np.vstack([self.w, inputVector])
                    self.clustNum = len(self.w)
                    self.i.append(0)
                    #forces new node selection
                    Twin = len(T)
                    T = np.append(T, 1)
        if self.i[Twin] < 1000000:    
            self.i[Twin] += 1        
        #update weights
        if not any(np.isnan(self.w[Twin])): 
            self.w[Twin] = self.beta*fuzzyAnd(inputVector, self.w[Twin]) + (1-self.beta)*self.w[Twin]   
        else:
            self.w[Twin] = np.array([inputVector]) 
        if getCluster == True:
            #output which node won
            return Twin

    def run(self, data, optNum = 1):
        #Identifies Cluster for data with ability to return sub optimal guesses
        #example: setting optNum = 2 returns second best guess
        inputVector = np.concatenate((data, 1-data))
        if self.w is None:
            self.w = np.array(inputVector)
            self.clustNum = 1
            self.i.append(0)
        else:
            self.clustNum = len(self.w)
        #determine input norm and match criterion
        normInput = norm(inputVector)
        MC = normInput*self.rho
        #Choose a category node
        T = np.zeros(self.clustNum)
        for j in range(0, self.clustNum):
            T[j] = norm(fuzzyAnd(inputVector,self.w[j]))/(self.alpha+norm(self.w[j]))
        #print(T)
        #find best match and set to zero to find optNum'th best guess 
        for i in range(0, optNum):
            hyp = 0
            Tg = np.copy(T)
            while (hyp < MC and norm(Tg) != 0):
                Twin = np.argmax(Tg)
                hyp = norm(fuzzyAnd(inputVector, self.w[Twin]))
                if hyp < MC:
                    Tg[Twin] = 0
            if i < optNum -1:
                T[Twin] = 0
            #print('removed--',norm(fuzzyAnd(inputVector, self.w[Twin])), MC)
        #reevaluate to find next best guess
        #print('guessed--',norm(fuzzyAnd(inputVector, self.w[Twin])), MC)
                
        #output second best guess
        return Twin

        
