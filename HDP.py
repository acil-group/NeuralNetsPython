#Author: Niklas Melton
#Date: 24/02/2016
#Heuristic Dynamic Programming general algorithm implementation. 

import numpy as np, math
from NeuralNets import MLP

def qualPrint(x):
    condList = [x<0, x>0, x==0]
    changeList = ['-','+','0']
    return np.select(condList,changeList)

class HDPnet:
    def __init__(self, shape, utility = None, model = None, actor = None, critic = None, growThresh = None):

        #expects shape to be a multi dimensional tupple
        #i.e. ((3,2,1),(4,3,3),(3,2,1))
        self.shape = shape
        if utility == None:
            print('Error: Utility function required')
            return
        if actor is None:
            actor = MLP.network((self.shape[0]), growThresh = growThresh)
        if model is None:
            model = MLP.network((self.shape[1]), growThresh = growThresh)
        elif model is False:
            #ADHDP variation
            print('ADHDP')
        if critic is None:
            critic = MLP.network((self.shape[2]), growThresh = growThresh)
        critic.minMax = False
        self.actor = actor
        self.model = model
        self.critic = critic
        self.utility = utility
        self.Xlast = None
        self.Xnext_last = None
        self.modelError = 1

        self.aLR = None
        self.mLR = None
        self.cLR = None
        self.aMR = None
        self.mMR = None
        self.cMR = None

        self.cycleA = 1
        self.cycleM = 1
        self.cycleC = 1

    def updateRates(self, LR, MR):
        if self.aLR is None:
            self.aLR = LR
        if self.aMR is None:
            self.aMR = MR
        if self.mLR is None:
            self.mLR = LR
        if self.mMR is None:
            self.mMR = MR
        if self.cLR is None:
            self.cLR = LR
        if self.cMR is None:
            self.cMR = MR

    def setRates(self, aLR, mLR, cLR, aMR, mMR, cMR):
        self.aLR = aLR
        self.mLR = mLR
        self.cLR = cLR
        self.aMR = aMR
        self.mMR = mMR
        self.cMR = cMR

    def clearModel(self):
        self.Xlast = None
        

    def judgeAction(self, A, X, LR, MR, gamma=0.3, trainModel = True, trainCritic = True):
        self.updateRates(LR, MR)
        Xa = np.concatenate((X,A),0)
        if self.model is not False:
            #find utility of current state
            U = self.utility(X)
            if trainModel is True and self.Xlast is not None:
                #update model
                for nM in range(0, self.cycleM):
                    self.modelTrain(self.Xlast,X, self.mLR, self.mMR)
            #estimate next state
            Xnext = self.model.feedForward(Xa)
            if self.Xnext_last is not None:
                self.modelError = 0.01*sum(abs((self.Xnext_last-X)))/len(X) + 0.99*self.modelError
            self.Xnext_last = Xnext
            #print(np.round(Xa,2),'\n',np.round(Xnext,2))
            if trainCritic is True:
                #critique current and future states
                Jnext = self.critic.feedForward(Xnext)
                J = self.critic.feedForward(X)
                #update critic to estimate the bellman equation
                E = J - (gamma*Jnext+U)
##                print('E',E)
##                print('J', J)
##                print('Jnext', Jnext)
##                print('U', U)
                self.critic.backProp(E, self.cLR, self.cMR)
            #update actor through critic and model
            dXnext = self.critic.backProp(np.array([1]))
            dXa = self.model.backProp(dXnext)
            dA = dXa[len(X):]
        else:
            #ADHDP
            J = self.critic.feedForward(Xa)
            if self.Xlast is not None:
                if trainCritic is True:
                    #update critic to estimate the bellman equation
                    Jlast = self.critic.feedForward(self.Xlast)
                    Ulast = self.utility(self.Xlast)
                    E = Jlast - (gamma*J+Ulast)
                    print(E, J, Ulast)
                    self.critic.backProp(E, self.cLR, self.cMR)
                    #get updated value
                    J = self.critic.feedForward(Xa)
                    
                dXa = self.critic.backProp(np.array([1]))
                dA = dXa[len(X):]
            else:
                dA = np.zeros(len(A))
        self.Xlast = Xa
        return np.copy(dA)

    def learnPass(self, X, LR, MR, gamma=0.3,getA = True, trainModel = True):
        #runs one learning pass of the HDP network
        #maxK determines the number of timesteps into the future we look
        Xlast = self.Xlast
        if LR != 0:
            for nA in range(0, self.cycleA):
                A = self.actor.feedForward(X)
                dA = judgeAction(A, X, LR, MR, gamma, trainCritic = False, trainModel = False)
                self.actor.backProp(dA, self.aLR, self.aMR)
            for nC in range(0, self.cycleC):
                self.Xlast = Xlast
                dA = judgeAction(A, X, LR, MR, gamma, trainCritic = True, trainModel = trainModel)
        if getA != False:
            return np.copy(A)


    def modelTrain(self, X_last, X, LR, MR):
        X_g = self.model.feedForward(X_last)
        error = (X_g - X)
        self.model.backProp(error, LR, MR)
        return error
            
            
                
                
            
                
                
            
