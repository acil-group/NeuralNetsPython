#Author: Niklas Melton
#Date: 04/04/2016
#HDP testing code

import pyDrive, HDP as HDP, MLP, pygame, sys, time, math, random, numpy as np
from pygame.locals import *

screenSize = 1000, 600


carSize = 40,20
senseNum = 11
speed = 1
steerRange = math.radians(60)
sRange = 100
LR = 0.3
MR = 0.0
gamma=0.1

my_course = pyDrive.course('kidney.txt')
trackModel = MLP.loadNetwork('model11.txt')
critic = MLP.loadNetwork('critic11.txt')

HDPshape = ((11,7,1),trackModel.shape,critic.shape)

def utility(X):
    cost = 0
    costMax = 0
    n = len(X)
    for i in range(0, int(n/2)):
        cost += abs(X[i]-X[n-1-i])
        costMax+=1
    cost += 0.2*np.sum((1-X))
    costMax += 0.2*n
    
    return np.copy(cost/costMax)


A = HDP.HDPnet(HDPshape, utility, model=trackModel, critic = critic)
my_vehicle = pyDrive.car(carSize, my_course.startPos)
my_env = pyDrive.env(screenSize, my_vehicle, my_course, senseNum, sRange)
my_env.update(speed, 0)
X = my_env.getSense()/sRange

while True:
    
    while my_env.crashed == False:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        
        steer = MLP.anorm(A.learnPass(X, LR, MR, gamma),-steerRange/2,steerRange/2)
        my_env.update(speed, steer)
        X = my_env.getSense()/sRange
        #A.modelTrain(LR,MR,X)
    print('crashed')
    my_env.reset()
    X = my_env.getSense()/sRange

    
