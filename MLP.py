#Author: Niklas Melton
#Date: 23/02/2016
#Basic MLP neural network library

import numpy as np, math

DCONST = 0.01    #constant error value to accelerate learning
MAXWEIGHT = 10
verb = False;

def sech(x):
    return 1/np.cosh(x)

def activeF(x):
    return np.tanh(np.pi*x)

def dActiveF(x):
    return pow(sech(x*np.pi),2)
    #return 1-pow(x,2)

def norm(X, Xmin,Xmax, angular = False):
    X -= Xmin
    X /= (Xmax-Xmin)
    if angular == True:
        X = round((X%1), 3)
    return X

def anorm(X, Xmin,Xmax):
    X *= (Xmax-Xmin)
    X += Xmin
    return X
mapMax = 0
def remap(x,xMin,xMax,newMin,newMax, angular = False):
    if(xMax == None):
        if mapMax == None or x> mapMax:
            xMax = mapMax
    x = norm(x,xMin,xMax, angular)
    anorm(x,newMin, newMax)
    return x

def BPerror(X, Xgoal):
    return (X*(1-X)+DCONST)*(X-Xgoal)

def LSerror(X, Xgoal):
    return math.sqrt(sum(pow((X-Xgoal),2)))

def estimateShape(inSize, outSize,cont = True):
    if (inSize > outSize):
        midSize = math.sqrt(pow(inSize,2)-pow(outSize,2))
    else:
        midSize = math.sqrt(pow(outSize,2)-pow(inSize,2))
    if cont is True:
        shape = [inSize,midSize,outSize]
    else:
        shape = [inSize,midSize,midSize,outSize]
    return shape

    

class network:
    def __init__(self, shape, weights = None, growThresh = None):
        self.growThresh = growThresh
        self.pruneThresh = 0.01
        self.avgLag = 2000
        self.i_lag = 0
        self.i_FF = 0
        self.errorAVG = 1
        self.errorAVGlast = 1
        self.instability = 1;
        self.weights = weights
        self.shape  = shape
        self.minMax = True
        self.bias = True
        self.DCONST = 0
        #expects shape of network in the form of a tuple or array for DG
        if self.shape is None:
            if weights is None:
                print("ERROR: must provide either shape or weights")
                return
            self.shape = []
            for i in range(0,len(weights)):
                self.shape.append(len(weights[i]))
        
        #create augmented (true) shape for bias nodes
        self.truShape = np.array(self.shape)+1
        self.truShape[-1] = self.shape[-1]
        if type(self.shape) == tuple:
            self.truShape = tuple(self.truShape)
        #define nodes and weights by shape
        self.nodes = np.zeros(len(self.truShape), dtype = object)
        self.errorNodes = np.zeros(len(self.truShape), dtype = object)
        for i in range(0, len(self.truShape)):
            self.nodes[i] = np.zeros(self.truShape[i], dtype = 'float32')
            #set bias nodes high
            self.nodes[i][-1] = 1
            self.errorNodes[i] = np.zeros(self.truShape[i], dtype = 'float32')
        if self.weights is None:
            #initialize new set of weights if not given
            self.weights = np.zeros((len(self.truShape)-1), dtype = object)
            for i in range(0, len(self.weights)):
                self.weights[i] = np.random.rand(self.shape[i+1],self.truShape[i])*0.1-0.05
        else:
            self.instability = 0
        #initialize weight memory
        self.Dweights = np.zeros((len(self.truShape)-1), dtype = object)
        for i in range(0, len(self.Dweights)):
            self.Dweights[i] = np.zeros((self.shape[i+1],self.truShape[i]))

    def copy(self, NN):
        self.growThresh = NN.growThresh
        self.pruneThresh = NN.pruneThresh
        self.avgLag = NN.avgLag
        self.i_lag = NN.i_lag
        self.i_FF = NN.i_FF
        self.errorAVG = 1.1*NN.errorAVG
        self.errorAVGlast = 1
        self.instability = 1;
        self.weights = NN.weights
        self.Dweights = np.zeros((len(self.truShape)-1), dtype = object)
        for i in range(0, len(self.Dweights)):
            self.Dweights[i] = np.zeros((self.shape[i+1],self.truShape[i]))
        self.shape  = NN.shape
        self.minMax = NN.minMax
        self.bias = NN.bias
        self.truShape = NN.truShape
        self.nodes = np.zeros(len(self.truShape), dtype = object)
        self.errorNodes = np.zeros(len(self.truShape), dtype = object)
        self.instability = 0
        self.DCONST =NN.DCONST
        
        
        

    def flushNodes(self):
        self.nodes = np.zeros(len(self.truShape), dtype = object)
        self.errorNodes = np.zeros(len(self.truShape), dtype = object)
        for i in range(0, len(self.truShape)):
            self.nodes[i] = np.zeros(self.truShape[i], dtype = 'float32')
            #set bias nodes high
            self.nodes[i][-1] = 1
            self.errorNodes[i] = np.zeros(self.truShape[i], dtype = 'float32')

    def biasPurge(self):
        self.bias = False
        #for each weight layer:
        for i in range(0, len(self.weights)):
            #for each node in the bottom of this weight layer (excluding bias)
            for j in range(0, self.shape[i+1]):
                self.weights[i][j][-1] = 0

            

    def feedForward(self, X):
        self.flushNodes()
        if self.i_FF < 1000000:
            self.i_FF += 1
        self.nodes[0] = np.append(X, 1)
        #for each weight layer:
        for i in range(0, len(self.weights)):
            #keep bias nodes high
            self.nodes[i][-1] = 1*self.bias
            #for each node in the bottom of this weight layer (excluding bias)
            for j in range(0, self.shape[i+1]):
                self.nodes[i+1][j] = 0
                #for each node in the top of this weight layer (including bias)
                for k in range(0, self.truShape[i]-1*(not self.bias)):
                    #sum the product of the weights and bottom nodes
                    #print(self.nodes[i].shape,self.nodes[i+1].shape,self.weights[i].shape)
                    self.nodes[i+1][j] += self.nodes[i][k]*self.weights[i][j][k]
                    self.nodes[i+1][j] = round(self.nodes[i+1][j], 5)
            
            if i != len(self.weights)-1:
                #take activeF of node values
                self.nodes[i+1] = activeF(self.nodes[i+1])
            elif self.minMax:
                #minmax
                self.nodes[i+1][self.nodes[i+1]>1] = np.array([1])
                self.nodes[i+1][self.nodes[i+1]<-1] = np.array([-1])
            else:
                if any(abs(self.nodes[i+1]) > 10):
                    print('------OVERVALUE------')
                    self.nodes[i+1][self.nodes[i+1]>10] = np.array([10])
                    self.nodes[i+1][self.nodes[i+1]<-10] = np.array([-10])
            
                
        if verb:
            print('=================================================================')
            print('Nodes\n',self.nodes)
            print('Weights\n',self.weights)
            
        #return last node layer
        return np.copy(self.nodes[-1])

        



    def backProp(self, error, LR = None, MR = 0.0):
        #keep track of error
        e = sum(abs(error))/len(error)
        if e != 1 and e != -1:
            self.errorAVG = e*0.0001+self.errorAVG*0.9999
        self.i_lag += 1
        
        self.instability -= (10/self.avgLag)*(0.5-self.errorAVG)
        if self.instability < 0:
            self.instability = 0

                
        #LR defines the learning rate while MR defines the Momentum Rate
        self.errorNodes[-1] = np.copy(error)
        #for each weight layer, counting backward:
        for i in range(len(self.weights)-1, -1, -1):
            #for each node (including bias)
            for j in range(0, self.truShape[i]-1*(not self.bias)):
                errorSum = 0
                #sum the product of the lower nodes and weights (excluding bias)
                for k in range(0, self.shape[i+1]):
                    errorSum += self.errorNodes[i+1][k]*self.weights[i][k][j]
                    if LR is not None:
                        #update weights
                        self.Dweights[i][k][j] = (1-MR)*self.errorNodes[i+1][k]*self.nodes[i][j] + MR*self.Dweights[i][k][j]
                        if abs(self.Dweights[i][k][j]) > 0.1*self.weights[i][k][j]+0.1:
                            self.Dweights[i][k][j] = np.sign(self.Dweights[i][k][j])*(0.1*self.weights[i][k][j]+0.1)
                        if abs(self.weights[i][k][j]) < MAXWEIGHT:
                            self.weights[i][k][j] += -LR*(self.Dweights[i][k][j] +self.DCONST)
                        else:
                            self.weights[i][k][j] = np.sign(self.weights[i][k][j])*MAXWEIGHT
                            self.Dweights[i][k][j] = 0
                self.errorNodes[i][j] = (dActiveF(self.nodes[i][j]))*errorSum

        if self.growThresh is not None:
            #check if node addition is recomended
            if self.i_lag >= self.avgLag:
                self.i_lag = 0
                #print('----------avgErr',(self.errorAVGlast-self.errorAVG),(self.growThresh*self.errorAVGlast))
                if all([(self.errorAVGlast-self.errorAVG) < (self.growThresh*self.errorAVGlast),
                np.sign((self.errorAVGlast-self.errorAVG)) == 1]):
                    if True:
                        print('----GROWTH----')
                    #add new node
                    self.instability = 1
                    for i in range(1, len(self.shape)-1):
                        self.shape[i] += 1
                        self.truShape[i] += 1
                    for i in range(1, len(self.truShape)-1):
                        self.nodes[i] = np.insert(self.nodes[i],len(self.nodes[i])-1,0)
                        self.errorNodes[i] = np.insert(self.errorNodes[i],len(self.errorNodes[i])-1,0)
                    #insert small random weights 
                    for i in range(1, len(self.weights)):
                        self.weights[i] = np.insert(self.weights[i], self.weights[i].shape[1]-1,(0.05*np.random.rand(self.weights[i].shape[0])-0.025),1)
                        self.weights[i-1] = np.insert(self.weights[i-1], self.weights[i-1].shape[0],(0.05*np.random.rand(self.weights[i-1].shape[1])-0.025),0)
                    for i in range(1, len(self.Dweights)):
                        self.Dweights[i] = np.insert(self.Dweights[i], self.Dweights[i].shape[1]-1,np.zeros(self.Dweights[i].shape[0]),1)
                        self.Dweights[i-1] = np.insert(self.Dweights[i-1], self.Dweights[i-1].shape[0],np.zeros(self.Dweights[i-1].shape[1]),0)
                #pruning phase
                else:
                    for i in range(1, len(self.weights)):
                        for k in range(self.shape[i]-1, -1, -1):
                            keep = False
                            for j in range(0, self.shape[i+1]):
                                if abs(self.weights[i][j][k]) > 0.001:
                                    keep = True
                                    break
                            if keep == False:
                                if True:
                                    print('----PRUNE----')
                                print('------',i,k,'--------')
                                self.weights[i] = np.delete(self.weights[i],k,1)
                                self.weights[i-1] = np.delete(self.weights[i-1],k,0)
                                self.Dweights[i] = np.delete(self.Dweights[i],k,1)
                                self.Dweights[i-1] = np.delete(self.Dweights[i-1],k,0)
                                self.shape[i] -= 1
                                self.truShape[i] -= 1
                                self.nodes[i] = np.delete(self.nodes[i],k)
                                self.errorNodes[i] = np.delete(self.errorNodes[i],k)                         
                self.errorAVGlast = self.errorAVG                            
            
        if verb:
            print('Error Nodes\n',self.errorNodes)
            print('Dweights\n',self.Dweights)
        #return the topmost error derivatives, removing bias node
        if LR is None:
            return np.delete(self.errorNodes[0], -1)

    def saveNetwork(self, filename):
        file = open(filename, 'w')
        print('Writing file: ',filename)
        file.write(str(self.shape))
        file.write('\n')
        for i in range(0, len(self.weights)):
            for j in range(0, len(self.weights[i])):
                for k in range(0, len(self.weights[i][j])):
                    file.write(str(self.weights[i][j][k]))
                    file.write('\n')
        file.close()
        print('Saved.')

    def defDynamicGrowth(self, growThresh = None, pruneThresh = None, avgLag = None):
        if growThresh is not None:
            self.growThresh = growThresh
        if pruneThresh is not None:
            self.pruneThresh = pruneThresh
        if avgLag is not None:
            self.avgLag = avgLag

    def supervised(self, X,S,LR,MR = None):
        A = self.feedForward(X)
        error = A -S
        self.backProp(error, LR, MR)
        return A


class pseudoNet:
    #A dummy class that allows regular functions to be used
    #as if it were a neural net. F is the function and G is it's
    #gradient function
    def __init__(self, F, G):
        self.F = F
        self.G = G
        self.X_last = None
        self.shape = 0

    def feedForward(self, X):
        self.X_last = X
        return np.array(self.F(X))

    def backProp(self, error, LR = None, MR = 0.0):
        return np.array(self.G(self.X_last, error))
        
        

def loadNetwork(filename):
    file = open(filename, 'r')
    print('Loading: ', filename)
    shape = eval(file.readline())
    weights = np.zeros(len(shape)-1, dtype=object)
    for i in range(0, len(weights)):
        weights[i] = np.zeros((shape[i+1],(shape[i]+1)))
        for j in range(0, len(weights[i])):
            for k in range(0, len(weights[i][j])):
                weights[i][j][k] = float(file.readline())
    file.close()
    print('Loaded.')
    return network(shape, weights)


    

class TDLnet:
    def __init__(self, nTDL, shape, nC=0,  weights = None, growThresh = None):
        self.nTDL = nTDL
        self.shape = shape
        self.NN = network(shape, weights, growThresh)
        self.nTX = (shape[0]-nC)/self.nTDL
        self.nC = nC
        self.Xlast = [np.zeros(self.nTX)]
        for n in range(1,self.nTDL):
            self.Xlast += [np.zeros(self.nTX)]
        self.XQ = 0
        

    def copy(self, TDLNN):
        self.nTDL = TDLNN.nTDL
        self.NN.copy(TDLNN.NN)
        self.Xlast = [None]
        for n in range(0,self.nTDL-1):
            self.Xlast += [None]

    def pushT(self, x):
        if self.nC != 0:
            x = x[:self.nTX]
        for n in range(self.nTDL-1,0,-1):
            self.Xlast[n] = self.Xlast[n-1]
        self.Xlast[0] = x

    def getXvec(self, Xnow):
        if self.nC != 0:
            Xconst = Xnow[-self.nC:]
        else:
            Xconst = np.array([])
        self.pushT(Xnow)
        Xvec = np.zeros(self.NN.shape[0])
        for n in range(0, self.nTDL):
            Xvec[n*self.nTX:(n+1)*self.nTX] = self.Xlast[n]
        Xvec[self.nTDL*self.nTX:] = Xconst
        return Xvec

    def feedForward(self, X):
        XTDL = self.getXvec(X)
        if self.XQ < self.nTDL:
            self.XQ += 1
            return np.zeros(self.shape[-1])
        else:
            return self.NN.feedForward(XTDL)

    def backProp(self, error, LR = None, MR = 0.0):
        if self.XQ == self.nTDL:
            return self.NN.backProp(error, LR, MR)
        elif LR is None:
            return np.zeros(len(self.NN.nodes[0])-1)
        else:
            return

    def supervised(self, X,S,LR,MR = None):
        A = self.feedForward(X)
        error = A -S
        self.backProp(error, LR, MR)
        return A

    def clearHistory(self):
        self.Xlast = [np.zeros(self.nTX)]
        for n in range(1,self.nTDL):
            self.Xlast += [np.zeros(self.nTX)]
        self.XQ = 0
        return

    def saveNetwork(self, filename):
        return self.NN.saveNetwork(filename)

        
        
        
        
        



    
