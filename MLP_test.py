#Author: Niklas Melton
#Date: 7/03/2016
#Basic MLP library test, approximates sin wave

import math, MLP, random, numpy as np, matplotlib.pyplot as plt


error = 1
avgError = 1
avgLast = 1
tol = 1e-3
random.seed()
A = MLP.network([1,2,1],growThresh = 0.01)
LR = 0.02
MR = 0.0


xLow = -np.pi
xHigh = np.pi
yLow = -1
yHigh = 1

pi = np.pi

def F(X):
    return min(max(np.sin(X*pi), -1),1)

def plotNet():
    xList = np.arange(-1,1,0.05)
    fList = np.zeros(len(xList))
    nList = np.zeros(len(xList))
    for i in range(0,len(xList)):
        fList[i] = F(xList[i])
        nList[i] = A.feedForward(xList[i])
    plt.plot(np.pi*xList,fList,'r--',np.pi*xList,nList,'b--')
    plt.show()

#begin training
i = 0
while (avgError > tol):
    i += 1
    Xnorm = 2*np.random.rand()-1
    #print('Test: ',X, F)
    zNet = A.feedForward(Xnorm)
    BPerror = zNet-F(Xnorm)
    A.backProp(BPerror, LR, MR)
    error = pow(BPerror,2)
    avgError = avgError*0.9999 + error*0.0001
    if (avgError < 0.9*avgLast):
        print(A.shape, LR ,avgError, 'iter:', i)
        avgLast = avgError
    
print(avgError, 'iter:', i)

plotNet()
    


