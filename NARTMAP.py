#Author: Niklas Melton
#Date: 27/06/2016
#(working title) piecewise neural network policy learner dynamically
#managed by ART

import numpy as np
from NerualNets import ARTMAP, EnhancedSL as ESL, NeuralART as NART


class NARTMAP:
    def __init__(self,rho,alpha,beta,NNshape=None,wA=None,wB=None,wMap = None,
                 wNN=None,growThresh=None):
        self.ARTMAP = ARTMAP.ARTMAP(rho,alpha,beta,wA,wB,wMap)
        self.ARTMAP.B = NART.NARTnet(rho,alpha,beta,NNshape,wB,wNN,growThresh)
        self.ESLnet = None
        self.active_clust = None
        
    def supervised(self, dataA, dataB, LR, MR getA = True):
        B = self.ARTMAP.supervised(dataA, dataB, getB = True)
        A = self.ARTMAP.B.NN[B].feedForward(dataA)
        error = MLP.BPerror(A, dataB)
        self.ARTMAP.B.NN.backProp(error, LR, MR)
        if getA is True:
            return A

    def setCritic(self, shape, utility, model = None, actor = None, critic = None):
        #expects shape to be a multi dimensional tupple
        #i.e. ((3,2,1),(4,3,3),(3,2,1))
        self.ESLnet = ESL.ESLnet(shape, utility, model, actor, critici)

    def ESL(self, dataA, dataB, K, LR, MR, gamma=0.3 getA = True, modelTrain = True):
        if self.ESLnet is None:
            print("ERROR: must call setCritic to initialize HDP")
            return

        cluster = self.ARTMAP.supervised(dataA, dataB, getB = True)
        if self.active_cluster is None:
            self.active_cluster = cluster
        elif self.active_cluster != cluster:
            self.active_cluster = cluster
            self.ESLnet.actor = self.ARTMAP.B.NN[self.active_cluster]

        A = self.ESLnet.learnPass(dataA, dataB, K, LR, MR, gamma, getA, modelTrain)
        if getA is True:
            return A

    def selfSupervised(self, dataA, LR, MR, gamma, getA, modelTrain = True):
        if self.ESLnet is None:
            print("ERROR: must call setCritic to initialize HDP")
            return
        #check if just initialized
        if self.active_cluster is None:
            cluster = self.ARTMAP.supervised(dataA, None, getB = True)
            self.active_cluster = cluster
            self.ESLnet.actor = self.ARTMAP.B.NN[self.active_cluster]
        else:
            #get preliminary net cluster guess using dataA
            cluster = self.ARTMAP.runForward(dataA, getW = False)

        #see if things have changed since last itme
        if self.active_cluster != cluster:
            #start using new cluster weights
            self.active_cluster = cluster
            self.ESLnet.actor = self.ARTMAP.B.NN[self.active_cluster]

        A = self.ESLnet.actor.feedForward(dataA)
        #check if control signal is unstable, dont cluster if it is
        if self.NN[self.active_cluster].instability > 0.2:
            #cluster with a place holder
            cluster = self.ARTMAP.supervised(dataA, None, getB = True)
        else:
            #get true net cluster using net output as dataB
            cluster = self.ARTMAP.supervised(dataA, A, getB = True)
        #see if cluster is out of resonance
        if self.active_cluster != cluster:
            #start using new cluster weights
            self.active_cluster = cluster
            self.ESLnet.actor = self.ARTMAP.B.NN[self.active_cluster]
            A = self.ESLnet.actor.feedForward(dataA)

        #continue learning
        dA = self.ESLnet.HDP.judgeAction(A, dataA, LR, MR, gamma, trainModel = modelTrain)
        self.ESLnet.actor.backProp(dA, LR, MR)
        if getA is True:
            return A
        
