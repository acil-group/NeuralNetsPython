#Author: Niklas Melton
#Date: 27/06/2016
#Piecwise Neural Nets defined by Fuzzy ART

from NeuralNets import  MLP, FuzzyART as ART
import numpy as np

class NARTnet:
    def __init__(self,rho,alpha,beta,NNshape=None,wART=None,wNN=None,growThresh=None):
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.ART = ART.ARTnet(self.rho,self.alpha,self.beta,wART)
        self.clustNum = self.ART.clustNum
        self.NN = np.array([],dtype=object)
        self.NNshape = NNshape
        self.growThresh = growThresh
        if NNshape is None and wART is None:
            print("ERROR: must provide shape to initialize new net")
            return
        if wART is not None:
            for w in range(0, len(wART)):
                if wNN is not None:
                    self.NN.append(MLP.network(NNshape, wNN[w], self.growThresh))
                else:
                    self.NN.append(MLP.network(NNshape, wNN, self.growThresh))
                

    def run(self, dataA, dataB=None):
        if dataB is None:
            dataB = dataA
        cluster = self.ART.run(dataA)
        NNout = self.NN[cluster].feedForward(dataB)
        return [cluster, NNout]

    def learnRun(self, dataA, getA = False, dataB = None):
        cluster = self.ART.learnRun(dataA)
        subCluster = self.ART.run(dataA)
        self.clustNum = self.ART.clustNum
        #check if new node was created
        if cluster >= len(self.NN):
            #if not first node
            if self.clustNum != 1:
                subCluster = self.ART.run(dataA, 2)
                #append until there are an equal number of nets and nodes
                while cluster >= len(self.NN):
                    #make copy of closest related network
                    newMLP = MLP.network(self.NNshape, growThresh = self.growThresh)
                    newMLP.copy(self.NN[subCluster])
                    self.NN = np.append(self.NN, newMLP)
                    self.NN[-1].instability = self.NN[subCluster].instability
            else:
                #initialize a blank network
                self.NN = np.append(self.NN, MLP.network(self.NNshape, growThresh = self.growThresh))
        if getA:
            if dataB is None:
                dataB = dataA
            return [cluster, self.NN[cluster].feedForward(dataB)]
        else:
            return cluster

    def backProp(self, cluster, error, LR, MR):
        self.NN[cluster].backProp(error, LR, MR)
        

