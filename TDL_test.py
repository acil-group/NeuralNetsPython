#Author: Niklas Melton
#Date: 7/03/2016
#Basic MLP library test, approximates sin wave

import math, MLP, random, numpy as np, matplotlib.pyplot as plt


error = 1
avgError = 1
avgLast = 1
tol = 1e-5
random.seed()
A = MLP.TDLnet(3,[3,2,2,1],growThresh = 0.01)
LR = 0.02
MR = 0.0

xLow = -np.pi
xHigh = np.pi
yLow = -1
yHigh = 1

def getX(X):
    return 2*((((X+1)/2)*263+71)%100)/100 - 1

X = np.random.rand()
#begin training
i = 0
while (avgError > tol):
    i += 1
    if LR > 1e-6:
        LR *= 0.999999
    #Xnorm = np.array(2*np.random.rand()-1)
    #F = math.sin(np.pi*Xnorm)
    #print('Test: ',X, F)
    #zNet = A.feedForward(Xnorm)
    zNet = A.feedForward(X)
    F = getX(X)
    X = F
    BPerror = zNet-F
    A.backProp(BPerror, LR, MR)
    error = pow(BPerror,2)
    avgError = avgError*0.9999 + error*0.0001
    if (avgError < 0.9*avgLast):
        print(A.shape, LR ,avgError, 'iter:', i)
        avgLast = avgError
    
print(avgError, 'iter:', i)

xList = np.arange(xLow,xHigh,0.05)
fList = np.zeros(len(xList))
nList = np.zeros(len(xList))
for i in range(0,len(xList)):
    fList[i] = math.sin(xList[i])
    nList[i] = A.feedForward(xList[i]/np.pi)
plt.plot(xList,fList,'r--',xList,nList,'b--')
plt.show()
exit
    


