#Author: Niklas Melton
#Date: 24/04/2016
#Critic trainer
import MLP, numpy as np, math

senseNum = 11

def utility(X):
    cost = 0
    costMax = 0
    n = len(X)
    for i in range(0, int(n/2)):
        cost += abs(X[i]-X[n-1-i])
        costMax+=1
    cost += 0.2*np.sum((1-X))
    costMax += 0.2*n
    
    return np.copy(cost/costMax)

critic = MLP.network((senseNum,15,1))
tol = 0.02;
avgError = 1
avgErrorLast = 1
i = 0
while avgError > tol:
    i += 1
    x = np.random.rand(senseNum)
    U = utility(x)
    J = critic.feedForward(x)
    BPerr = MLP.BPerror(J,U)
    error = MLP.LSerror(J,U)
    avgError = (avgError*100 + error)/101
    critic.backProp(BPerr, 0.4, 0.8)
    if avgError < 0.9*avgErrorLast:
        print('Error: ',avgError, 'iter: ',i)
        avgErrorLast = avgError
critic.saveNetwork('critic11.txt')
    
