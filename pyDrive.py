#Author: Niklas Melton
#Date: 24/02/2016
#env car and course basic code

import pygame, math, sys, os, numpy as np

white = 255,255,255
black = 0, 0, 0
yellow = 255,255,0
red = 255,0,0
blue = 0,0,255
green = 0,255,0

screenPos = [0, 25]


class env:
    def __init__(self, screenSize, vehicle, course = None, senseNum = 0, sRange = 0):
        #course and car must both be predefined objects
        self.course = course
        self.vehicle = vehicle
        self.senseNum = senseNum
        self.sensors = np.zeros((senseNum,2))
        self.range = sRange
        self.crashed = False
        self.JK = False
        self.initPos = vehicle.initPos
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (screenPos[0],screenPos[1])
        pygame.init()
        self.screen = pygame.display.set_mode(screenSize)


    def sense(self, draw = True):
        sAngle = math.pi/(self.senseNum+1)
        s = 0
        for sensor in self.sensors:
            sensor[0] = self.range
            sensor[1] = self.vehicle.theta + ((s+1)*sAngle) - math.pi/2
            s += 1
        for sensor in self.sensors:

            for segment in self.course.path:
                
                parallel = False
                if segment[0] < segment[1]:
                    lxMin = segment[0]
                    lxMax = segment[1]
                else:
                    lxMin = segment[1]
                    lxMax = segment[0]
                if segment[2] < segment[3]:
                    lyMin = segment[2]
                    lyMax = segment[3]
                else:
                    lyMin = segment[3]
                    lyMax = segment[2]
                if abs(lxMax-lxMin) > 0:
                    #find m and b of line if not vertical
                    mLine = (segment[3]-segment[2])/(segment[1]-segment[0])
                    bLine = segment[2] - mLine*segment[0]
                    
                    if abs(sensor[1]) == math.pi/2 or abs(sensor[1]) == 3*math.pi/2:
                        #if sensor line is vertical
                        xhit = self.vehicle.X
                    else:
                        #otherwise find m and b of sensor
                        mSense = math.tan(sensor[1])
                        bSense = self.vehicle.Y - mSense*self.vehicle.X
                        
                        if mSense != mLine:
                            #if lines are not parallel
                            xhit = (bSense-bLine)/(mLine-mSense)
                        else:
                            parallel = True
                    
                    if parallel == False:
                        #find collision point
                        yhit = mLine*xhit + bLine
                else:
                
                    #if line is vertical
                    if abs(sensor[1]) == math.pi/2 or abs(sensor[1]) == 3*math.pi/2:
                        #if sensor is also vertical
                        parallel = True
                    else:
                        #otherwise, find collision point
                        xhit = lxMax
                        mSense = math.tan(sensor[1])
                        bSense = self.vehicle.Y - mSense*self.vehicle.X
                        yhit = mSense*xhit + bSense

                if parallel == False:
                    R = math.hypot((xhit-self.vehicle.X),(yhit-self.vehicle.Y))
                    if round(math.atan2((yhit-self.vehicle.Y),(xhit-self.vehicle.X)),2) == round(sensor[1],2) or round(math.atan2((yhit-self.vehicle.Y),(xhit-self.vehicle.X))+2*math.pi,2) == round(sensor[1],2) or round(math.atan2((yhit-self.vehicle.Y),(xhit-self.vehicle.X))-2*math.pi,2) == round(sensor[1],2):
                        #check if ahead of sensor
                        if R < sensor[0] and xhit <= lxMax and xhit >= lxMin and yhit <=lyMax and yhit >= lyMin:
                            sensor[0] = R
                
            
        if draw == True:
            for sensor in self.sensors:
                thetaS = sensor[1]
                xS = self.vehicle.X + sensor[0]*math.cos(thetaS)
                yS = self.vehicle.Y + sensor[0]*math.sin(thetaS)
                pygame.draw.line(self.screen,red,(self.vehicle.X,self.vehicle.Y),(xS,yS),2)
        for sensor in self.sensors:
            if sensor[0] < 1:
                self.crashed = True

    def getSense(self):
        S = np.zeros(len(self.sensors))
        s = 0
        for sensor in self.sensors:
            S[s] = sensor[0]
            s += 1
        return S
    
    def reset(self):
        self.vehicle.X = self.initPos[0]
        self.vehicle.Y = self.initPos[1]
        self.vehicle.theta = self.initPos[2]
        if len(self.initPos) > 3:
            self.vehicle.theta_trail = self.initPos[3]
        self.crashed = False
        self.JK = False
          
    def update(self, speed, steer, vehicleColor = red, courseColor = black):
        #print(self.vehicle.theta)
        self.vehicle.drive(speed,steer)
        self.screen.fill(white)
        if self.course is not None:
            self.course.draw(self.screen, courseColor)
        self.sense()
        if hasattr(self.vehicle, 'JK'):
            self.JK = self.vehicle.JK
        else:
            self.JK = False
        self.vehicle.draw(self.screen, vehicleColor)
        pygame.display.flip()

def newCourse(screen):

    screenSize = screen.get_size()

    course = []
    polyCount = 1
    print('StartLine!')
    xa = int(input('x1'))
    xb = int(input('x2'))
    ya = int(input('y1'))
    yb = int(input('y2'))
    startLine = [xa,xb,ya,yb]
    prevLine = startLine
    screen.fill(white)
    pygame.draw.line(screen, red, (startLine[0],startLine[2]), (startLine[1], startLine[3]), 2)
    pygame.display.flip()
    while(1):
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys. exit()
        print('NewLine! previousend value was:', prevLine[1],prevLine[3])
        cont = input('continue side?')
        if cont == 'y':
            xa = prevLine[1]
            xb = int(input('nextX'))
            ya = prevLine[3]
            yb = int(input('nextY'))
        else:
            xa = input('x1')
            if xa == 'n':
                break
            else: xa = int(xa)
            xb = int(input('x2'))
            ya = int(input('y1'))
            yb = int(input('y2'))
            polyCount += 1
        line = [xa,xb,ya,yb]

        pygame.draw.line(screen, black, (line[0],line[2]), (line[1], line[3]), 2)
        pygame.display.flip()
        approve = input('keep? (y/n)')
        if approve == 'y':
            course.append(line)
            prevLine = line
            coord = font.render((str(line[1])+','+str(line[3])), 2, red)
            screen.blit(coord, (line[1],line[3]))
            
        else:
            pygame.draw.line(screen, white, (line[0],line[2]), (line[1], line[3]), 2)
    save = input('save? (y/n)')
    if save == 'y':
        fileName = input('file name? ')
        file = open(fileName, 'w')
        file.write(str(screenSize)+'\n')
        file.write(str(polyCount)+'\n')
        segmentString = str()
        for p in startLine:
            segmentString += str(p)
        segmentString += '\n'
        file.write(segmentString)
        for seg in course:
            segmentString = str()
            for p in seg:
                segmentString += str(p)
            segmentString += '\n'
            file.write(segmentString)
        file.close()

    return
    

class course:
    def __init__(self, courseFile):
        #imports course as a file listing lines of 3 digit coordinates in sets of 4
        #corresponding to [x1,x2,y1,y2] with no spaces or deliminators aside from \n.
        #converts this file to an object with same coordinate system
        file = open(courseFile, 'r')
        self.screenSize = eval(file.readline())
        polyNum = int(file.readline())
        self.path  = np.array([])
        self.pathPoly = np.zeros(polyNum, dtype=object)
        n = 0
        
        for i in file:
            line = str(i)
            if n == 0:
                segment = np.array([int(line[0:3]),int(line[3:6]),int(line[6:9]), int(line[9:12])])
                self.startLine = segment
                
                if self.startLine[0] > self.startLine[1]:
                    startX = ((self.startLine[0]-self.startLine[1])/2)+self.startLine[1]
                    xAng = self.startLine[0]-self.startLine[1]
                else:
                    startX = ((self.startLine[1]-self.startLine[0])/2)+self.startLine[0]
                    xAng = self.startLine[1]-self.startLine[0]
                if self.startLine[2] > self.startLine[3]:
                    startY = ((self.startLine[2]-self.startLine[3])/2)+self.startLine[3]
                    yAng = self.startLine[2]-self.startLine[3]
                else:
                    startY = ((self.startLine[3]-self.startLine[2])/2)+self.startLine[2]
                    yAng = self.startLine[3]-self.startLine[2]

                startTheta = math.atan2(yAng, xAng) - math.pi/2

                self.startPos = [startX, startY, startTheta, startTheta]
                n+=1
            else:
                segment = np.array([int(line[0:3]),int(line[3:6]),int(line[6:9]), int(line[9:12])])
                if n == 1:
                    self.path = np.array(segment)
                    n += 1
                else:
                 self.path = np.vstack((self.path, segment))

        n = 0
        p = 0
        for segment in self.path:
            pointA = np.array([segment[0],segment[2]])
            pointB = np.array([segment[1],segment[3]])
            #print(p, pointA, pointB)
            if n == 0:
                poly = np.array(pointA)
                startPoint = pointA
                n += 1
                
            elif pointB[0] != startPoint[0] or pointB[1] != startPoint[1]:
                poly = np.vstack((poly,pointA))
            else:
                poly = np.vstack((poly,pointA))
                self.pathPoly[p] = poly
                p += 1
                n = 0
                
        

    def draw(self, screen, color):
        pygame.draw.line(screen,red,(self.startLine[0],self.startLine[2]),(self.startLine[1],self.startLine[3]),3)
        for p in self.pathPoly:
            pygame.draw.polygon(screen,color,p,2)

class car:
    def __init__(self, size, iPos):
        #size = (L,W)
        #iPos = (X,Y,theta)
        self.initPos = iPos
        self.L = size[0]
        self.W = size[1]
        self.X = iPos[0]
        self.Y = iPos[1]
        self.theta = iPos[2]
        self.XB = self.X - self.L*math.cos(self.theta)
        self.YB = self.Y - self.L*math.sin(self.theta)
        
    def drive(self, speed, steer):
        #steer should be in radians
        self.XB = self.X - self.L*math.cos(-self.theta)
        self.YB = self.Y - self.L*math.sin(self.theta)
        self.theta += (speed*math.tan(steer))/self.L
        if abs(self.theta)>math.pi:
            self.theta -= 2*math.pi*(self.theta/abs(self.theta))

        self.XB += (speed*math.cos(-self.theta))
        self.YB += (speed*math.sin(self.theta))

        self.X = self.XB + self.L*math.cos(-self.theta)
        self.Y = self.YB + self.L*math.sin(self.theta)

        
        

    def draw(self, screen, color):
        xb = self.X - self.L*math.cos(self.theta)
        yb = self.Y - self.L*math.sin(self.theta)
        xRect = 0.5*self.W*math.sin(-self.theta)
        yRect = 0.5*self.W*math.cos(-self.theta)
        xLB = xb+xRect
        yLB = yb+yRect
        xRB = xb-xRect
        yRB = yb-yRect
        xLF = self.X+xRect
        yLF = self.Y+yRect
        xRF = self.X-xRect
        yRF = self.Y-yRect
        pygame.draw.polygon(screen, color, [(xLB,yLB),(xRB,yRB),(xRF,yRF),(xLF,yLF)])


class truck:
    def __init__(self, size, iPos):
        #size = ((L_cab,W_cab),(L_trailer,W_trailer))
        #iPos = (X,Y, theta, theta_trail)
        iPos[3] += math.pi
        self.initPos = iPos
        self.L_cab = size[0][0]
        self.W_cab = size[0][1]
        self.L_trail = size[1][0]
        self.W_trail = size[1][1]
        self.X = iPos[0]
        self.Y = iPos[1]
        self.theta = iPos[2]
        self.theta_trail = iPos[3]
        self.JK = False
        self.X_pivot = self.X - self.L_cab*math.cos(-self.theta)
        self.Y_pivot = self.Y - self.L_cab*math.sin(self.theta)
        self.X_trail = self.X_pivot - self.L_trail*math.cos(self.theta_trail)

    def drive(self, speed, steer):
        #move according to vehicle kinematics
        #move cab
        self.X_pivot = self.X - self.L_cab*math.cos(-self.theta)
        self.Y_pivot = self.Y - self.L_cab*math.sin(self.theta)
        self.theta += (speed*math.tan(steer))/self.L_cab
        if abs(self.theta)>math.pi:
            self.theta -= 2*math.pi*(self.theta/abs(self.theta))
        self.X_pivot += (speed*math.cos(-self.theta))
        self.Y_pivot += (speed*math.sin(self.theta))
        self.X = self.X_pivot + self.L_cab*math.cos(-self.theta)
        self.Y = self.Y_pivot + self.L_cab*math.sin(self.theta)
        
        #move trailer
        self.theta_trail += speed*math.sin(self.theta_trail - self.theta)/self.L_trail
        if abs(self.theta_trail)>math.pi:
            self.theta_trail -= 2*math.pi*(self.theta_trail/abs(self.theta_trail))
        self.X_trail =  self.X_pivot + self.L_trail*math.cos(-self.theta_trail)
        self.Y_trail = self.Y_pivot + self.L_trail*math.sin(self.theta_trail)
        self.X = self.X_pivot + self.L_cab*math.cos(-self.theta)
        self.Y = self.Y_pivot + self.L_cab*math.sin(self.theta)

        #check if truck is jackknifed
        JKangle = self.theta - self.theta_trail
        if abs(JKangle)>math.pi:
            JKangle -= 2*math.pi*(JKangle/abs(JKangle))
        if abs(JKangle) < math.pi/2:
            self.JK = True
        else:
            self.JK = False
        

    def draw(self, screen, color):
        L_front = self.L_cab - 0.5*self.W_cab*math.sin(math.pi/4)
        #cab
        X_cab_BR = self.X_pivot + math.hypot(0.5*self.W_cab,0.5*self.W_cab)*math.cos((math.pi/4)+self.theta)
        Y_cab_BR = self.Y_pivot + math.hypot(0.5*self.W_cab,0.5*self.W_cab)*math.sin((math.pi/4)+self.theta)
        X_cab_FR = X_cab_BR + L_front*math.cos(self.theta)
        Y_cab_FR = Y_cab_BR + L_front*math.sin(self.theta)
        X_cab_BL = self.X_pivot + math.hypot(0.5*self.W_cab,0.5*self.W_cab)*math.cos((-math.pi/4)+self.theta)
        Y_cab_BL = self.Y_pivot + math.hypot(0.5*self.W_cab,0.5*self.W_cab)*math.sin((-math.pi/4)+self.theta)
        X_cab_FL = X_cab_BL + L_front*math.cos(self.theta)
        Y_cab_FL = Y_cab_BL + L_front*math.sin(self.theta)
        cabPoly = [(self.X_pivot,self.Y_pivot),(X_cab_BR,Y_cab_BR),(X_cab_FR,Y_cab_FR),(X_cab_FL,Y_cab_FL),(X_cab_BL,Y_cab_BL)]
        
        #trailer
        X_trail_FR = self.X_pivot + 0.5*self.W_trail*math.cos((math.pi/2)+self.theta_trail)
        Y_trail_FR = self.Y_pivot + 0.5*self.W_trail*math.sin((math.pi/2)+self.theta_trail)
        X_trail_FL = self.X_pivot + 0.5*self.W_trail*math.cos((-math.pi/2)+self.theta_trail)
        Y_trail_FL = self.Y_pivot + 0.5*self.W_trail*math.sin((-math.pi/2)+self.theta_trail)
        X_trail_BR = self.X_trail + 0.5*self.W_trail*math.cos((math.pi/2)+self.theta_trail)
        Y_trail_BR = self.Y_trail + 0.5*self.W_trail*math.sin((math.pi/2)+self.theta_trail)
        X_trail_BL = self.X_trail + 0.5*self.W_trail*math.cos((-math.pi/2)+self.theta_trail)
        Y_trail_BL = self.Y_trail + 0.5*self.W_trail*math.sin((-math.pi/2)+self.theta_trail)
        trailPoly = [(X_trail_FR,Y_trail_FR),(X_trail_BR,Y_trail_BR),(X_trail_BL,Y_trail_BL),(X_trail_FL,Y_trail_FL)]

        #draw polygons
        pygame.draw.polygon(screen, color, cabPoly)
        pygame.draw.polygon(screen, color, trailPoly)


class bot:
    def __init__(self, size, iPos):
        #size [L,W]
        #iPos [X, Y, theta]
        self.initPos = iPos
        self.L = size[0]
        self.W = size[1]
        self.X = iPos[0]
        self.Y = iPos[1]
        self.theta = iPos[2]
        
    def drive(self, speed, steer):
        #bot steering is based on differential drive kinematics
        #therefore, steer here is not taken in radians, but rather as a float from (-1.0)-(1.0)
        #steering value of 0.0 is straight while -1.0 is turning in place left
        #and 1.0 is turning in place right
        VR = speed
        VL = speed
        if steer > 0:
            VL = 2*(0.5-steer)*speed 
        elif steer < 0:
            VR = 2*(0.5+steer)*speed
            
        if steer != 0:
            X_mid = self.X - 0.5*self.L*math.cos(self.theta)
            Y_mid = self.Y - 0.5*self.L*math.sin(self.theta)
            X_mid += ((VR+VL)/(VR-VL))*(self.W/2)*(math.sin(((VR-VL)/self.W)+self.theta)-math.sin(self.theta))
            Y_mid -= ((VR+VL)/(VR-VL))*(self.W/2)*(math.cos(((VR-VL)/self.W)+self.theta)-math.cos(self.theta))

            self.theta += (VR-VL)/self.W
            if abs(self.theta)>math.pi:
                    self.theta -= 2*math.pi*(self.theta/abs(self.theta))

            self.X = X_mid + 0.5*self.L*math.cos(self.theta)
            self.Y = Y_mid + 0.5*self.L*math.sin(self.theta)
        else:
            self.X += speed*math.cos(self.theta)
            self.Y += speed*math.sin(self.theta)

    def draw(self, screen, color):
        xb = self.X - self.L*math.cos(self.theta)
        yb = self.Y - self.L*math.sin(self.theta)
        xRect = 0.5*self.W*math.sin(-self.theta)
        yRect = 0.5*self.W*math.cos(-self.theta)
        xLB = xb+xRect
        yLB = yb+yRect
        xRB = xb-xRect
        yRB = yb-yRect
        xLF = self.X+xRect
        yLF = self.Y+yRect
        xRF = self.X-xRect
        yRF = self.Y-yRect
        pygame.draw.polygon(screen, color, [(xLB,yLB),(xRB,yRB),(xRF,yRF),(xLF,yLF)])
        
                                                
                        
            
            
        
    
        
                
        
        
        
    
